class dummy::config inherits dummy {
    if $facts['operatingsystem'] == 'debian' {
    file { '/var/run/ntp/servers-netconfig':
      ensure => 'absent'
    }
  }
}