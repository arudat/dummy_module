class dummy::install inherits dummy {
  notify{"----------on my way ---------------": }

  package { 'fish':
    ensure => 'installed',
  }

}
